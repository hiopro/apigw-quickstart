# apigw-quickstart

#### 项目介绍
基于 [go-kit](https://github.com/go-kit/kit) 微服务工具集，实现基于 gRPC 协议或 RestFul 的 API 网关样例。

包含：网关日志、网关度量、服务调用鉴权，服务协议转换，服务降级、服务发现、服务链路追踪

#### 软件架构
- common (服务通用组件)
- config (TOML 格式配置文件解析组件)
- pb (gRPC 服务定义)
- adapters (后台服务的适配器，利用 golang 的 plugin 模式实现)
- cmd
  - api-gateway (服务端指令)

#### 软件流程图

![bootstrap](bootstrap.png)

![access_flow](access_flow.png)

#### 安装方法

git clone https://gitee.com/jason_elva8325/apigw-quickstart.git

#### 命令及参数

```sh
Usage of api-gateway:
  -adapter-dir string
    	Adapter files store location
  -conf-file string
    	Host path of TOML format config file. If this param set, other params will be lose efficacy
  -graceful-timeout duration
    	Graceful shutdown timeout (default 15s)
  -http-ca-cert string
    	HTTPS root cert file
  -http-cert string
    	HTTPS cert file
  -http-key string
    	HTTPS cert key file
  -http-port string
    	HTTP listen port
  -listen-host string
    	TCP listen host (default "0.0.0.0")
  -srd-etcd-ca-cert string
    	ETCD client access root cert file path for Service Discovery & Registry
  -srd-etcd-cert string
    	ETCD client access cert file path for Service Discovery & Registry
  -srd-etcd-key string
    	ETCD client access cert‘s key file path for Service Discovery & Registry
  -srd-etcd-servers string
    	Comma-separated ETCD server addresses for Service Discovery & Registry. Example: '127.0.0.1:2379,127.0.0.1:13279' (default "127.0.0.1:2379")
  -srd-prefix string
    	Service Discovery & Registry Prefix (default "/svc/")
  -verify-client-cert
    	Force verify client certification
```

##### 使用说明

1. -conf-file 参数如果一旦设定，则其它参数均不起作用，TOML 配置文件样例，详见 config 目录
2. -http-port 参数如果不设定，则启用 HTTP 或 HTTPS 的默认端口
3. -srd-etcd-* 参数必须一并设置，否则不能连接 ETCD 服务器
4. -adapter-dir 参数如果不设置，则默认为命令所在目录
5. -http-cert 和 -http-key 参数必须成对设置，才能启用 SSL
6. -verify-client-cert 参数只有 SSL 启用后设置才有效
7. -verify-client-cert 参数如果设置为 `false`，则客户端调用时不需要使用客户端证书密钥对；如果为 `true` 则必须使用客户端证书密钥对进行调用
8. -http-ca-cert 参数仅当 HTTP SSL 证书为私有自签名证书时，才有必要指定

#### Adapter 编写要求

1. 必须在 adapters 目录下建立新的目录，且不能再包含子目录，该目录名称将作为 adapter 文件的文件名
2. adapter 的目录下必须建立 main.go 文件，该文件内要求 package 必须为 main，且必须继承 `func AdapterRegistry(router *mux.Router, logger log.Logger, client etcdv3.Client, srdPrefix string)` 方法，详见 adapters 目录下的样例程序
3. 使用 adapters 目录下的 gen.sh 文件编译生成 adapter 文件。该脚本的使用方法如下：`gen.sh all` 代表构建所有 adapters，`gen.sh [adapter 目录名]` 代表构建指定的 adapter

#### Command 构建脚本说明

使用 cmd 目录下的 gen.sh 脚本构建命令文件。该脚本的使用方法如下：`gen.sh all` 代表构建所有 Command，`gen.sh [command 目录名]` 代表构建指定的 command，构建生成的命令文件名同 `[command 目录名]`

#### 依赖条件

- ETCD 服务作为注册发现服务
- prometheus 作为服务度量服务

#### 其它说明

本API网关在请求报文头中产生的 sessionid 为追踪 ID，通过在服务链路中各服务按照时序输出的日志信息作为追踪信息实现