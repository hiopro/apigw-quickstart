package common

// APIResume API描述信息
type APIResume struct {
	Route            string `json:"route"`
	PathRegexp       string `json:"path-regexp"`
	QueriesTemplates string `json:"queries-templates"`
	QueriesRegexps   string `json:"queries-regexps"`
	Methods          string `json:"methods"`
}

type GenericResponse struct {
	V   interface{} `json:"v"`
	Err string      `json:"err"`
}
