#!/bin/sh
dstDir=$(cd `dirname $0`; pwd)
protoc -I=$dstDir/../../micro-quickstart/pb --go_out=plugins=grpc:$dstDir \
    $dstDir/../../micro-quickstart/pb/*.proto
