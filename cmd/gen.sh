#!/bin/sh
dstDir=$(cd `dirname $0`; pwd)

if [ $# -ne 1 ]; then
    echo "\nError: there must be only one parameter for generate command name, or use 'all' to generate all commands.\n";
    exit 1
fi

mkdir -p $dstDir/../dist

if [ "$1" != "all" ]; then
    do_cmd="go build -o $dstDir/../dist/$1"
    for t_file in $(ls $dstDir"/"$1 | grep \.go)
    do
        do_cmd="$do_cmd $dstDir/$1/$t_file"
    done
    `$do_cmd`
else
    cnt=0
    for t_dir in $(ls -F $dstDir | grep '/$')
    do
        outFile="$dstDir/../dist/${t_dir/\//}"
        do_cmd="go build -o $outFile"
        for t_file in $(ls $dstDir"/"$t_dir | grep \.go)
        do
            do_cmd="$do_cmd $dstDir/$t_dir/$t_file"
        done
        `$do_cmd`
        cnt=$[$cnt+1];
    done
    echo "$cnt commands generated.";
fi
