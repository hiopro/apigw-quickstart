package main

import (
	"net/http"

	"gitee.com/jason_elva8325/apigw-quickstart/common"
)

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do stuff here
		logger.Log("Request URL", r.RequestURI, "sessionid", r.Header.Get("Sessionid"))
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}

func headerInitMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// 为每次请求添加sessionID
		sessionID := common.RandomSessionID()
		r.Header.Set("Sessionid", sessionID)
		// 设置默认响应数据格式
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}
